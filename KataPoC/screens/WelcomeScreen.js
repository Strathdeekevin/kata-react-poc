import React from 'react';
import {Button, View, StyleSheet} from 'react-native'

const WelcomeScreen = ({navigation}) => {
    return (
      <View>
        <Button
      title="Go to counter"
      onPress={() =>
        navigation.navigate('Counter', { name: 'Jane' })
      }
    />
      </View>
        
    );
}

export default WelcomeScreen;