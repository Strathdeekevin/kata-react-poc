import React, { useState } from "react";
import { View, StyleSheet } from 'react-native';
import Header from '../components/Header';
import Counter from '../components/Counter';

const CounterScreen = ({ navigation }) => {
  const [count, setCount] = useState(0);

  const incrementCount = () => setCount(count + 1);
  const decrementCount = () => setCount(count - 1);

  return (
    <View style={styles.container}>
      <Header />
      <Counter
        count={count}
        incrementCount={incrementCount}
        decrementCount={decrementCount}
      />
    </View>
  );
};
const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingTop:40,
      backgroundColor: '#fff',
    },
  });
export default CounterScreen;
