import React from 'react';
import { View,Text, StyleSheet } from 'react-native';

const Header = ({title}) => {
    return (
        <View style={styles.header}>
            <Text style={styles.title}>
                {title}
            </Text>
        </View>
    );
}

Header.defaultProps = {
    title: 'Kata'
}

const styles = StyleSheet.create({
    header:{
        height : 60,
        backgroundColor:'green',
        padding : 15,
    },
    title:{
        color:'white',
        fontSize: 23,
        textAlign: 'center'
    }
})
export default Header;