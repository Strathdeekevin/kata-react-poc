import { View, Text, StyleSheet, Button } from "react-native";
import React from "react";

function Counter(props) {

    return (
        <View style={styles.container}>
            <Text style={styles.text}>
                count: {props.count}
            </Text>
            <View style={styles.buttonContainer}>
                <Button title ='increment' onPress={()=> props.incrementCount()}/>
                <Button title = 'decrement' onPress={()=>props.decrementCount()}/>
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:"center",
        alignContent:"center",
    },
    buttonContainer:{
        flexDirection:"row",
        justifyContent:"center",
        alignContent:"center",
    },
    text:{
        fontSize:20,
        textAlign: 'center'
    }
})
export default Counter;